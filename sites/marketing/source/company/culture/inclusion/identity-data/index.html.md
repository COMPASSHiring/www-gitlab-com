---
layout: handbook-page-toc
title: "Identity data"
description: "GitLab country specific data in regard to team members location, gender, ethnicity, race, age etc. View data here!"
canonical_path: "/company/culture/inclusion/identity-data/"
---

#### GitLab Identity Data

Data as of 2020-12-31

##### Country Specific Data

| **Country Information**                     | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Based in APAC                               | 145       | 11.51%          |
| Based in EMEA                               | 343       | 27.22%          |
| Based in LATAM                              | 21        | 1.67%           |
| Based in NORAM                              | 751       | 59.60%          |
| **Total Team Members**                      | **1,260** | **100%**        |

##### Gender Data

| **Gender (All)**                            | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men                                         | 867       | 68.81%          |
| Women                                       | 393       | 31.19%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **1,260** | **100%**        |

| **Gender in Management**                    | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Management                           | 153       | 68.00%          |
| Women in Management                         | 72        | 32.00%          |
| Other Gender Management                     | 0         | 0%              |
| **Total Team Members**                      | **225**   | **100%**        |

| **Gender in Leadership**                    | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Leadership                           | 72        | 72.73%          |
| Women in Leadership                         | 27        | 27.27%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **99**    | **100%**        |

| **Gender in Engineering**                   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Engineering                          | 435       | 79.67%          |
| Women in Engineering                        | 111       | 20.33%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **546**   | **100%**        |

##### Race/Ethnicity Data

| **Race/Ethnicity (US Only)**                | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 1         | 0.14%           |
| Asian                                       | 47        | 6.68%           |
| Black or African American                   | 17        | 2.41%           |
| Hispanic or Latino                          | 36        | 5.11%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 30        | 4.26%           |
| White                                       | 437       | 62.07%          |
| Unreported                                  | 136       | 19.32%          |
| **Total Team Members**                      | **704**   | **100%**        |

| **Race/Ethnicity in Engineering (US Only)** | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 18        | 8.78%           |
| Black or African American                   | 3         | 1.46%           |
| Hispanic or Latino                          | 7         | 3.41%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 9         | 4.39%           |
| White                                       | 136       | 66.34%          |
| Unreported                                  | 32        | 15.61%          |
| **Total Team Members**                      | **205**   | **100%**        |

| **Race/Ethnicity in Management (US Only)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 9         | 6.47%           |
| Black or African American                   | 2         | 1.44%           |
| Hispanic or Latino                          | 5         | 3.60%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 6         | 4.32%           |
| White                                       | 87        | 62.59%          |
| Unreported                                  | 30        | 21.58%          |
| **Total Team Members**                      | **139**   | **100%**        |

| **Race/Ethnicity in Leadership (US Only)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 9         | 11.54%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 0         | 0.00%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 3         | 3.85%           |
| White                                       | 50        | 64.10%          |
| Unreported                                  | 16        | 20.51%          |
| **Total Team Members**                      | **78**    | **100%**        |

| **Race/Ethnicity (Global)**                 | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 1         | 0.08%           |
| Asian                                       | 125       | 9.92%           |
| Black or African American                   | 30        | 2.38%           |
| Hispanic or Latino                          | 64        | 5.08%           |
| Native Hawaiian or Other Pacific Islander   | 2         | 0.16%           |
| Two or More Races                           | 38        | 3.02%           |
| White                                       | 707       | 56.11%          |
| Unreported                                  | 293       | 23.25%          |
| **Total Team Members**                      | **1,260** | **100%**        |

| **Race/Ethnicity in Engineering (Global)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 63        | 11.54%          |
| Black or African American                   | 10        | 1.83%           |
| Hispanic or Latino                          | 26        | 4.76%           |
| Native Hawaiian or Other Pacific Islander   | 1         | 0.18%           |
| Two or More Races                           | 15        | 2.75%           |
| White                                       | 305       | 55.86%          |
| Unreported                                  | 126       | 23.08%          |
| **Total Team Members**                      | **546**   | **100%**        |

| **Race/Ethnicity in Management (Global)**   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 16        | 7.11%           |
| Black or African American                   | 2         | 0.89%           |
| Hispanic or Latino                          | 7         | 3.11%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 7         | 3.11%           |
| White                                       | 140       | 62.22%          |
| Unreported                                  | 53        | 23.56%          |
| **Total Team Members**                      | **225**   | **100%**        |

| **Race/Ethnicity in Leadership (Global)**   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 10        | 10.10%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 1         | 1.01%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 3         | 3.03%           |
| White                                       | 60        | 60.61%          |
| Unreported                                  | 25        | 25.25%          |
| **Total Team Members**                      | **99**    | **100%**        |

##### Age Distribution

| **Age Distribution (Global)**               | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| 18-24                                       | 15        | 1.19%           |
| 25-29                                       | 202       | 16.03%          |
| 30-34                                       | 356       | 28.25%          |
| 35-39                                       | 283       | 22.46%          |
| 40-49                                       | 277       | 21.98%          |
| 50-59                                       | 108       | 8.57%           |
| 60+                                         | 19        | 1.51%           |
| Unreported                                  | 0         | 0.00%           |
| **Total Team Members**                      | **1,260** | **100%**        |

**Of Note**: `Management` refers to Team Members who are *People Managers*, whereas `Leadership` denotes Team Members who are in *Director-level positions and above*.

**Source**: GitLab's HRIS, BambooHR
